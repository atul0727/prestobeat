from expose import get_metrics_from_presto, publish_metrics_to_elastic
from clients import presto_host, beacon_elastic
import os
from datetime import datetime

print("#########################################################################")
print("started cron on : {}\n".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S")))
print("#########################################################################")

metriclist = get_metrics_from_presto(host=presto_host)
print(len(metriclist))
metriclist = publish_metrics_to_elastic(beacon_elastic, metriclist)
print(len(metriclist))

print("#########################################################################")
print("stopped cron on : {}\n".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S")))
print("#########################################################################")


