from elasticsearch import Elasticsearch
import os

env = os.environ.get("BEACON_CONFIG", default= "prod")
print(env)
password = os.environ.get("BEACON_PASSWORD", default= "changeme")
print(password)

if env == 'local':
    beacon_elastic = Elasticsearch("elastic:{}@localhost:9200".format(password))
    presto_host = "127.0.0.1:8080"
else:
    beacon_elastic = Elasticsearch("elastic:{}@beacon-elastic:9200".format(password))
    presto_host = "beacon-presto:8080"