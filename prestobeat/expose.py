import requests
import re
import csv
import elasticsearch.helpers

field_list = ["elapsedTime", "executionTime", "totalCpuTime", "totalScheduledTime", "queuedTime",
              "userMemoryReservation", "peakTotalMemoryReservation","totalMemoryReservation", "rawInputDataSize", "peakUserMemoryReservation"]


def get_metrics_from_presto(host):
    headers = {"X-Presto-User": "prestobeat"}
    response = requests.get("http://{}/v1/query/?state=FINISHED".format(host), headers=headers)
    if response.status_code == 200:
        print("presto API succesfully called")
        return response.json()
    else:
        print("presto API call failed")


def publish_metrics_to_elastic(elastic, metriclist):
    metriclist = dissect(metriclist)
    elasticsearch.helpers.bulk(elastic, metriclist)
    return metriclist


def send_msg_slack(msg, url="https://hooks.slack.com/services/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"):
    payload = {}
    payload["username"] = "PrestoBeat"
    payload["text"] = msg
    response = requests.post(url, payload)
    if response.status == 200:
        print("sent error on slack")
    else:
        print("sent notification failed")
        

def dissect(metriclist):
    
    dissect_metriclist = []

    for i, each in enumerate(metriclist):

        query_id = each['queryId']
        if "userID: " in each['query']:
            try:
                userID = each['query'].split(" ")[3]
                each['userID'] = userID
            except Exception as e:
                send_msg_slack(e)
            
        tmp = {}
        tmp["_index"] = "prestobeat"
        tmp["_type"] = "metrics"
        tmp["_id"] = query_id
        tmp["_source"] = each
        
        each = tmp

        for every in field_list:
            field = each['_source']['queryStats'][every]
            match = re.compile("[^\W\d]").search(field)
            number, string = field[:match.start()], field[match.start():]
            if string == "us":
                number = float(number)
            elif string == "ms":
                number = round(float(number) * 1000, 2)
            elif string == "s":
                number = round(float(number) * 1000000, 2)
            elif string == "m":
                number = round(float(number) * 60000000, 2)
            elif string == "M":
                number = round(float(number) * 1000000, 2)
            elif string == "B":
                number = round(float(number), 2)
            else:
                print("unhandled time format:{}".format(time))
                number = 0
            each['_source']['queryStats'][every] = number
        dissect_metriclist.append(each)

    print("dissection_complete")
    return dissect_metriclist